% this function performs the PCA analysis as described in our report document
function [recognition recognizedIndex recognizedImage]=pcaalgorithm(datapath,testImage)
    a=dir([datapath '*.jpg']) % match all the jpg files in the training data directory

    % get all training images, convert to grayscale & reshape, and add to array X
    X = [];
    for i=1:length(a);
        imgPath = strcat(datapath,int2str(i),'.jpg');
        img = imread(imgPath);
        img = rgb2gray(img);
        [height width] = size(img);
        % single quote operator https://nl.mathworks.com/help/matlab/ref/transpose.html
        temp = reshape(img',height*width,1);  
        X = [X temp];                
    end
    % get the average face
    m = mean(X,2);
    imgcount = size(X,2);

    % Calculate the differences between each image and the average image
    differences = [];
    for i=1 : imgcount
        temp = double(X(:,i)) - m;
        differences = [differences temp];
    end

    L = differences' * differences;
    [V,D]=eig(L); % returns diagonal matrix D of eigenvalues and matrix V whose columns are the corresponding right eigenvectors, so that differences*V = V*D.

    eigenVectorL = [];
    for i = 1 : size(V,2) 
            eigenVectorL = [eigenVectorL V(:,i)];
    end
    eigenfaces = differences * eigenVectorL;
    projectimg = [ ]; 
    for i = 1 : size(eigenfaces,2)
        temp = eigenfaces' * differences(:,i);
        projectimg = [projectimg temp];
    end
    testImage = testImage(:,:,1);
    [height width] = size(testImage);
    temp = reshape(testImage',height*width,1); 
    temp = double(temp)-m; 
    projtestimg = eigenfaces'*temp; 
    euclideDistances = [ ];
    for i=1 : size(eigenfaces,2)
        temp = (norm(projtestimg-projectimg(:,i)))^2;
        euclideDistances = [euclideDistances temp];
    end

    [euclideMinDistance recognizedIndex] = min(euclideDistances);
    recognition = euclideMinDistance/1.0e+17;
    recognizedImage = strcat(int2str(recognizedIndex),'.jpg');
end
