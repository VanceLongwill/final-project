% We wrote this function  to capture an image from video & process it in the way we want
% The images returned by our webcams were low quality and underexposed so we searched how to fix that
% the properties we wanted to manipulate unfortunately were not available so we decided to use our phone cameras to capture the images and preprocess them later
function img = captureFaceImage(vidsource)
    % vs = getselectedsource(vidsource);
    % get(vs) % show available properties for this video interface e.g. exposure
    rawImage = getsnapshot(vidsource);  

    FaceDetector = vision.CascadeObjectDetector();
    BBOX = step(FaceDetector, rawImage);
    % crop face
    croppedFace = imcrop(rawImage, BBOX(1,:)); %+ 100;

    hsvImage = rgb2hsv(croppedFace);  %# Convert the image to HSV space
    hsvImage(:,:,2) = hsvImage(:,:,2)*1.4 % increase saturation
    rgbImage = hsv2rgb(hsvImage);  %# Convert the image back to RGB space
    % detect face
    % resize cropped image to 200x200 and return it
    img = imresize(rawImage, [200 200]);
end
