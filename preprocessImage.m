function img = preprocessImage(rawImage)
    % detect face
    FaceDetector = vision.CascadeObjectDetector();
    BBOX = step(FaceDetector, rawImage);
    % crop face
    croppedFace = imcrop(rawImage, BBOX(1,:)); %+ 100;
    % resize cropped image to 200x200 and return it
    img = imresize(croppedFace, [200 200]);
end
