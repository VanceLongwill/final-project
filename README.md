## PCA Facial Recognition Smart Doorbell Proof of Concept

### Requirements

- MATLAB_R2018b
- Image acquisition toolbox

### Steps for setting up the project to use a video stream from your phone over the local network

1) Install the IP Webcam app for android: [More here](https://play.google.com/store/apps/details?id=com.pas.webcam)

2) Open the app & start the server

3) Find the url on your phone on the local network & the port number then
change the `baseUrl` variable as below in `recognize.m`

    ```baseUrl = 'http://192.168.2.2:8080'```

4) The code uses imread directly to obtain the snapshot image

    ```A = imread(url);```

- NB: you can also use your laptops webcam, below we provide an example

#### Alternative - Using the laptop webcam

```
    % open webcam
    vid = videoinput('macvideo',1); % platform dependent 
    set(vid, 'ReturnedColorSpace', 'RGB'); % get an rgb image which we can
    manipulate easily
    src = getselectedsource(vid); % we used this object to examine what properties we could change to get a better quality input from our webcam
    start(vid); % open video stream
    A = getsnapshot(vid); % get a snapshot from the video input stream
    stop(vid); % close video stream
```

Problems we encountered with this method:
	- Webcam photos unreliable
		- Sometimes black, distorted
		- Doesn't cope well with light changes
		- Cant configure exposure time (on MacOS using 'macvideo')
		- Noisy images with low resolution

- Using higher quality images in good lighting for both training the pca algorithm, and live matching resulted in much more reliable results.

## Training data

The PCA algorithm compares the main features of each the images in the `datapath` directory against the average features of all the images and finds the image with matching feature profiles to the individual faces in the current input image.

We use the `populateTrainingData` function to take all the images from the source directory and perform a number of manipulations to increase the homogeneity of our training data. The `preprocessImage` function performs these manipulations on a single image. All the output images should therefore be comparable 1 to 1. The source images must be jpeg images and each image must only contain one face. We found our results more reliable when all the images were taken in similar lighting conditions and at similar angles. Although for a more complex requirements set this might be an issue, for our doorbell we would expect the general angle of the input images to be the same.

In the `determineWho` function we decide what to show the end user based on the results of our PCA analysis. If the recognition value is below a certain value we return the name of the recognised face.



## Running the MATLAB project

- Open matlab
- Navigate to the root folder of the project
- In the command window, run:

    ```
    >>> recognize
    ```

- The program will attempt to find results indefinitely (polling every 2 seconds) until you press `<Ctrl> + c`
- The current image fetched from the device will be displayed only if faces are recognised
- The image will be annotated with an "Access Allowed" or "Access Denied" banner based on whether the face recognised is present in the list `allowedUsers`

