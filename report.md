In order to model the behaviour of our IoT doorbell we chose to use our
android phones. The features necessary in a tailor made IoT device for this
purpose were already present and ready to use in our android phones. An
example IoT camera doorbell might include

- A control unit to execute instructions
- A Camera for video input
- Networking capabilities to communicate with a central server 

In real life, the server might be in a remote location but as this is beyond
the scope of this project we looked for simpler solutions. Communications with
our remote server would also likely require us to deploy our own custom
application to the end device. We found ready-made solutions which met our
requirements for testing the core classification process so we stayed focused
on the task at hand. We installed the IP Webcam app for android which
served video from our phones' cameras on the local network. The classification
and authentication process could then be done using matlab on our laptops.

