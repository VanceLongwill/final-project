% this function takes the matched photo index and similarity rating as inputs and returns a name based on the rules made up by the cases below
% this pattern can be extended with more cases and more faces, providing the naming convention of this project is followed (i.e. 31.jpg, 32.jpg, 34.jpg...)
% we believe that increasing the number of faces in the training data would allow more accurate identification
function res = determineWho(x, guess)
    % if our match is strong enough
    x
    guess
    if (guess < 0.4)
        % the first 10 images
        if (x < 10) 
            res='Vance';
        % the next 10 images
        elseif (x > 9 && x < 21)
            res='Dónal';
        % the next 10 images
        elseif (x > 19 && x < 31)
            res='Özge';
        end
    else
        res = 'Unrecognized';
end
