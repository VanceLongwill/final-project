function recognize()
    % the directory where the 200x200 images to match against can be found
    datapath = './training-data/'; 
    % the local url of your android phone on the local network + port (default 8080)
    baseUrl = 'http://192.168.43.1:8080';
    % to get the snapshot jpeg not the continuous video stream
    path = "/shot.jpg"; 
    % append the path to the baseUrl to get the full local address of the endpoint
    url = strcat(baseUrl, path); 
    allowedUsers = ["Vance" "Ozge"];

    while (1)
        [resultImage, foundUser] = runRecognition(url, datapath);
        if ~isempty(foundUser)
            imshow(resultImage); % show the resulting annotated image
            % display a title showing the authentication status of the recognised face
            if ismember(foundUser, allowedUsers)
                title('Access allowed');
            else    
                title('Access denied');
            end
        else
        end
        % wait for 2 seconds before attempting to recognize someone again
        pause(2);
    end
end

function [res, name] = runRecognition(url, datapath)
    % relative path to our training images
    for i=1:5 % loop 5 times
        try
            % get the live image snapshot from the android device
            A = imread(url);
            % run our pca algorithm on each face
            [res, name] = getResults(A, datapath);
            if ~isempty(name) % if we have a face in view
                return;
            else
                disp("No faces found, trying again");
            end
        catch ME
            ME
            disp("Device failure, trying again");
        end
    end
    disp("Gave up after 5 attempts. Exiting");
    name = '';
    res = 0;
end

% this function can be used to test a single image, A, or alternative training sets by specifying a different datapath
function [imageOutput, personName] = getResults(A, datapath)
    % detect face
    FaceDetector = vision.CascadeObjectDetector();
    % draw box around detected face
    BBOX = step(FaceDetector, A); 
    % number of faces detected
    facecount = size(BBOX,1);
    recognizedFaces = []; 
    baseimg = [];

    % if any faces have been identified
    if facecount > 0
        % loop for each face
        for count=1:facecount
            % crop the image around the face only
            I2 = imcrop(A,BBOX(count,:));
            % get the size of the cropped image
            [h, w] = size(I2); 
            % expect a face to be over 200x200 as the user will stand close to the camera
            if h > 200 && w > 200 
                I2 = imresize (I2,[200 200]); % resize image for comparison
                % run comparison algorithm
                [recognition, recognisedImageIndex, recognisedImageFile] = pcaalgorithm(datapath,I2); % run pca analysis on test image vs pictures in the datapath provided
                % append the index found
                baseimg = [baseimg recognisedImageIndex];
                % append the recognition confidence
                recognizedFaces = [recognizedFaces recognition];
            end
        end

        % construct a matrix of the names of the identified faces to label the image with
        word = cell(1); 
        for i=1:length(recognizedFaces)
            % use the recognised image to determine which one of us is recognised
            foundPerson = determineWho(baseimg(i), recognizedFaces(i));
            % add the result to the annotations matrix
            word(i) = {foundPerson};
        end
        % the resulting image
        imageOutput = insertObjectAnnotation(A,'rectangle', BBOX, word,'TextBoxOpacity',0.8,'FontSize',30);
        % the names of those identified
        personName = word;
    else
        % return nothing if there are no faces present
        imageOutput = 0;
        personName = '';
    end
end
