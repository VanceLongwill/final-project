% takes photo from webcam
% function p = populateTrainingData(startIndex) % fills folderPath with 10 images in the right format    
%     % open webcam
%     vid = videoinput('macvideo',1);
%     set(vid, 'ReturnedColorSpace', 'RGB');
%     start(vid);
% 
%     folderPath = './training-data/'; % make sure this folder exists
%     extension = '.jpg';
% 
%     for i=startIndex:startIndex+9
%         % get current frame
%         resim = getsnapshot(vid);  
%         % detect face
%         FaceDetector = vision.CascadeObjectDetector();
%         BBOX = step(FaceDetector, resim);
%         % crop face
%         croppedFace = imcrop(resim, BBOX(1,:));
%         % resize cropped image to 180x200
%         resizedResim = imresize(croppedFace, [200 180]);
%         % construct filename
%         fileName = strcat(folderPath, num2str(i), extension);
%         % save image
%         imwrite(resizedResim, fileName);
%         % wait half a second
%         pause(0.5);
%     end
% end

% takes all the images from the source directory and converts them to the format used by the pca algorithm
function p = populateTrainingData() % fills folderPath with 10 images in the right format    
    targetFolderPath = './training-data/'; % make sure this folder exists
    sourceFolderPath = './source-data/'; % make sure this folder exists and is full of images
    a=dir([sourceFolderPath '*.jpg']); % match all the jpg files in the training data directory
    imgcount=size(a,1); % get the number of jpg files
    extension = '.jpg';

    for i=1:imgcount % preprocess all images
        % construct filenames
        % NB: files should be named in ascending numerical order as this index will later be used to identify and match the results
        sourceFileName = strcat(sourceFolderPath, int2str(i), extension);
        targetFileName = strcat(targetFolderPath, int2str(i), extension);

        % read the image from file
        rawImage = imread(sourceFileName);
        % find the face, crop, and resize
        img = preprocessImage(rawImage);
        % save image
        imwrite(img, targetFileName);
    end
end
