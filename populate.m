% An example of how to use our preprocessor
% Since we chose to use 10 images per person, this convenience function processes 10 images at a time
% Process images named 1.jpg to 10.jpg and saves the results to the training data folder
% populateTrainingData(1);
% Process images named 11.jpg to 20.jpg
% populateTrainingData(10);
% Process images named 21.jpg to 30.jpg
% populateTrainingData(20);


